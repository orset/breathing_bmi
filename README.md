# Study Name: Breathing affects motor imagery-based Brain Computer Interfaces

##Set-up
EEG Acquisition was done using 64 electrodes (ANTneuro, eegoSports). Breathing signals were acquired using a respiration belt (Piezo Film Effort Sensor – Kit 1389). 
The complete acquisition can be done by downloading [Neurodecode](https://github.com/fcbg-hnp/NeuroDecode). Follow the instruction on github to install the software on your computer. 
Before starting to record EEG signals, make sure that you can see the respiration and the heart signal (AUX channels). You will have to change the BIP mask in order to be able to record additional channels. By default it will record only the EEG.


## Protocol 

The protocol can be launched in two steps: 
1. Run the python script to detect in real-time the breathing phase: 
`python cue_trigger.py`
2. Run the python script of the MI protocol: 
`python protocol_MI_ON_OFF_offline_fixcross.py`

The parameter for all the experiment can be found in the ini file: protocol_configuration_MI.ini

## Event ID

- START [event-id = 10]: The trial is starting, the screen shows the current trial number.
- HOLD [event-id = 20]: REST period, subject is asked to stay still and wait for the cue to change (no anticipation)
- START_TASK [event-id = 30]: After 4s, the detection is listened and wait for the correct breathing phase to deliver the cue. 
- MI_START [event-id = 40]: The cue turn to green indicating to the subject to start MI. The color change is triggered randomly (time-onset cue)
- MI_START_INHALE [event-id = 43]: The cue turn to green indicating to the subject to start MI. The color change is triggered by the exhalation phase of the subject (inhale breathing-phase onset cue)
- MI_START_EXHALE [event-id = 44]: The cue turn to green indicating to the subject to start MI. The color change is triggered by the exhalation phase of the subject (exhale breathing-phase onset cue)
- MI_STOP_INHALE [event-id = 50]: The cue turn to red indicating to the subject to stop MI. The color change is triggered by the inhalation phase of the subject (inhale breathing-phase offset cue)
- MI_STOP_EXHALE [event-id = 51]: The cue turn to red indicating to the subject to stop MI. The color change is triggered by the exhalation phase of the subject (exhale breathing-phase offset cue)
- MI_STOP [event-id = 55]: The cue turn to red indicating to the subject to start MI. The color change is triggered randomly (time-offset cue).
- RELAX [event-id = 70]: Tue cue turn to grey. The trial is finished, a RELAX appear during 3s. The subject can move his hands and blinks.


## Analysis

All the scripts are based on jupyter notebook
Breathing_Doit: Launch chosen analysis over subjects

## Subject Script:
- Analysis_BandPower: calculate bandpower useful for mediation analysis (Fig 3, paper)
- Analysis_BandPower_Phase_Coupling: gives power values of bandpower for phase coupling  (Fig 2B, paper)
- Analysis_BandPower_Phase_Coupling_Topoplot: gives topoplot bandpower for phase coupling (Fig 2B, paper)
- Analysis_Breathing: extract phase and amplitude of breathing (Fig 3B, 4, 5A, paper)
- Analysis_ERDS_ICA: Calculate ERD/S (Fig 2A, paper)
- ExportingData_to_matlab: export data in matlab format for classification

## Grand Average Script:
- GA_analysis_ERDS_Phase_Coupling  (Fig 2B, paper)
- GA_analysis_ERDS  (Fig 2A)
- GA_analysis_ERDS_Phase_Coupling_Topoplot  (Fig 2B, paper)
- GA_Breathing_RespirationRate (Fig 3B)
- GA_Breathing_Cue: Plot breathing amplitude signal aligned on breathing cues (Fig S4, paper)
- GA_Breathing_MI_period: Plot breathing phase during Mi period (Fig S5, paper)


## Additional Matlab Scripts (MatlabDecoding folder > projects):

- Breathing_Doit.m:launch for each subject the analysis
- Breathing_Doit_GA.m: launch the GA analysis
- Decoding onset transition.m : train decoding on time-cue + breathing cue
- GA_exhale_inhale_onset.m: Grand Average Analysis over subjects for decoding between inhale and exhale (Fig 5B). 
